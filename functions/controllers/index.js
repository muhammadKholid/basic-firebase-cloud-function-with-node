const algoliasearch = require('algoliasearch');
const algoliaClient = algoliasearch(process.env.APPLICATIONID, process.env.ALGOLIA_API_KEY);

const { db } = require('../firebase/firebase');

//http://localhost:5000/avostore-dev/us-central1/app/v1/hello-world
const getHelloWorld = (_, res) => {
  res.status(200).send('Hello World!');
};

//check this api with this link
//http://localhost:5000/avostore-dev/us-central1/app/v1/orders
const getOrders = async (_, res) => {
  await db
    .collection('orders')
    .get()
    .then((ordersQuerySnapshot) => {
      const orders = [];
      ordersQuerySnapshot.forEach((doc) => {
        orders.push(doc.data());
      });
      res.status(200).json(orders);
    })
    .catch((err) => {
      console.log(err);
    });
};

//check this api with this link
//http://localhost:5000/avostore-dev/us-central1/app/v1/orders
//{
//"search": "AVS-1597209811-394"
//}
const getOrderByIdAlgolia = (req, res) => {
  const searchData = req.body.search;
  const index = algoliaClient.initIndex('orders');

  index
    .search(searchData, { attributesToRetrieve: ['buyerId', 'products'] })
    .then(({ hits }) => {
      res.status(200).json(hits);
    })
    .catch((err) => {
      console.log(err);
    });
};

//check this api with this link
//http://localhost:5000/avostore-dev/us-central1/app/v1/orders/AVS-1597209811-394
const getOrdersByIdThroughParams = (req, res) => {
  const { orderId } = req.params;
  const index = algoliaClient.initIndex('orders');

  index
    .search(orderId)
    .then(({ hits }) => {
      res.status(200).json(hits);
    })
    .catch((err) => {
      console.log(err);
    });
};

//check this api with this link
//http://localhost:5000/avostore-dev/us-central1/app/v1/orders/0
const getOrderPer10Page = (req, res) => {
  const { page } = req.params;
  const index = algoliaClient.initIndex('orders');

  index
    .search({
      hitsPerPage: 10,
      page,
    })
    .then(({ hits }) => {
      res.status(200).json(hits);
    })
    .catch((err) => {
      console.log(err);
    });
};

// const getOrderByFilters = (req, res) => {
//   const index = algoliaClient.initIndex('orders');
//   // const filters = 'orderState:SUBMITTED';

//   index
//     .search({
//       hitsPerPage: 10,
//       page: 0,
//     })
//     .then(({ hits }) => {
//       res.status(200).json(hits);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// };

module.exports = {
  getHelloWorld,
  getOrders,
  getOrderByIdAlgolia,
  getOrdersByIdThroughParams,
  getOrderPer10Page,
  // getOrderByFilters,
};
