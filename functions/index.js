require('dotenv').config();
const express = require('express');
const cors = require('cors');

const Routers = require('./routers/index');
const { functions } = require('./firebase/firebase');

const app = express();
const v1 = express.Router();

app.use(cors({ origin: true }));

//versioning for v1
v1.use('/v1', Routers);

app.use('/', v1);

exports.app = functions.https.onRequest(app);
//perminute
exports.scheduledFunction = functions.pubsub.schedule('* * * * *').onRun((context) => {
  console.log('This will be run every 1 minutes!');
  return null;
});
//perhour
exports.scheduledFunction = functions.pubsub.schedule('0 * * * *').onRun((context) => {
  console.log('This will be run every an hour!');
  return null;
});
//peronce a week in date 1
exports.scheduledFunction = functions.pubsub.schedule('0 0 1 * *').onRun((context) => {
  console.log('This will be run every once in a month in the first date!');
  return null;
});
