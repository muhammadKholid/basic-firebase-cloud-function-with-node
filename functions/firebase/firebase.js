const functions = require('firebase-functions');
const admin = require('firebase-admin');

var serviceAccount = require('../avostore-dev-adminsdk.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://avostore-dev.firebaseio.com',
});

const db = admin.firestore();

module.exports = { db, functions };
